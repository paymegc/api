const base64_decode = (value) => (new Buffer(value, 'base64')).toString('ascii')

module.exports = base64_decode