const base64_encode = (value) => Buffer.from(value).toString('base64')

module.exports = base64_encode