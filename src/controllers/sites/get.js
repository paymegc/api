require('module-alias/register')
const db = require("@app/db")

const getSites = async (req,res) => {

    const sites = await db.get({
        table:"tokens",
    })

    res.send({
        type:"ok",
        sites
    })
}
module.exports = getSites