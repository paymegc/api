require('module-alias/register')
const jwt = require('jsonwebtoken')

const base64_encode = require("@functions/base64_encode")

const dotenv = require('dotenv').config()
const env = dotenv.parsed

const getTokens = (req,res) => {
    try { 
        const {
            token,
            ip,
            domain
        } = req.query

        const token64 = base64_encode(token)
        const ip64 = base64_encode(ip)
        const domain64 = base64_encode(domain)

        const split = env.split

        const newToken = jwt.sign({ token64,ip64,domain64 }, split);

        return res.send({
            "type":"ok",
            token : newToken
        })
    } catch (error) {
        return res.status(500).send({
            "type":"error",
            error,
            "msj":`${error}`
        }) 
    }
}
module.exports = getTokens