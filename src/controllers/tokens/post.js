require('module-alias/register')
const jwt = require('jsonwebtoken')

const base64_decode = require("@functions/base64_decode")
const db = require("@app/db")

const dotenv = require('dotenv').config()
const env = dotenv.parsed

const postTokens = async (req,res) => { 
    try { 
        const {
            token 
        } = req.body
    
        const split = env.split
        const tokenDecode = jwt.verify(token, split);
    
        const tokenc = base64_decode(tokenDecode.token64)
        const ip = base64_decode(tokenDecode.ip64)
        const domain = base64_decode(tokenDecode.domain64)
    
        const data = {
            token : tokenc,
            ip,
            domain
        }
    
        const r = await db.put({
            table:"tokens",
            where:{
                domain:domain
            },
            data:{
                $set:{
                    ...data
                }
            },
            options:{
                "upsert":true
            }
        })
    
        res.send({
            "type":"ok",
            "msj":"validation ok",
            r
        })
    } catch (error) {
        return res.status(500).send({
            "type":"error",
            error,
            "msj":`${error}`
        }) 
    }
}
module.exports = postTokens