require('module-alias/register')

const sites  = (routes) => {
    routes.get(
        "/sites",
        [require("@middlewares/validateApiKey")],
        require("@controllers/sites/get")
    )
}
module.exports = sites