require('module-alias/register')

const tokens  = (routes) => {
    const validateGet = {
        token : {
            type : "string"
        },
        domain : {
            type : "string"
        },
        ip : {
            type : "string"
        },
    }
    routes.get(
        "/",
        [require("@middlewares/validateItem")(validateGet,"query")],
        require("@controllers/tokens/get")
    )
    const validatePost = {
        token : {
            type : "string"
        },
    }
    routes.post(
        "/",
        [
            require("@middlewares/validateItem")(validatePost),
            require("@middlewares/validateToken")
        ],
        require("@controllers/tokens/post")
    )
}
module.exports = tokens