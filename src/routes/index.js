require('module-alias/register')

const router = require('express').Router()

require("@routes/tokens")(router)
require("@routes/sites")(router)

module.exports = router
