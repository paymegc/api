const dotenv = require('dotenv').config()
const env = dotenv.parsed

const validateApiKey = (req,res,next) => {
    const {
        apikey 
    } = req.headers
    if(apikey !== env.APIKEY){
        return res.status(400).send({
            "type":"error",
            "msj":"apikey invalid"
        })  
    }
    next()
}
module.exports = validateApiKey