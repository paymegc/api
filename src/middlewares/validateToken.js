const jwt = require('jsonwebtoken')

const dotenv = require('dotenv').config()
const env = dotenv.parsed

const validateToken  = (req,res,next) => {
    const {
        token 
    } = req.body
    try {
        const split = env.split
        jwt.verify(token, split);
    } catch(error) {
        return res.status(400).send({
            "type":"error",
            error,
            "msj":`${error}`
        })  
    }
    next()
}
module.exports = validateToken